<?php
declare (strict_types=1);

namespace Yukebang\OpenApi\Result;

use Psr\Http\Message\ResponseInterface;
use Yukebang\OpenApi\Exception\ResponseException;

class ResponseResult
{
    protected ResponseInterface $rawResponse;

    protected $parsedData = null;
    /**
     * @var mixed|null
     */
    protected mixed $requestId;

    protected $isOk = false;

    public function __construct(ResponseInterface $response)
    {
        $this->rawResponse = $response;
        $this->parseResponse();
    }

    private function parseResponse()
    {
        $body = $this->getRawResponse()->getBody()->getContents();

        $this->parseBody($body);
    }

    private function parseBody(string $body)
    {
        try {
            $decode = json_decode($body, true, 512, 0 | JSON_THROW_ON_ERROR);
        } catch (\Throwable $exception) {
            throw new ResponseException($exception->getMessage(), $exception->getCode(), $exception);
        }

        $this->parseData($decode);
    }

    private function parseData(mixed $body)
    {
        $this->parsedData = $body;

        $this->requestId = $body['request_id'] ?? null;

        $this->isOk = $body['code'] === 1;
    }

    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    public function getRequestId()
    {
        return $this->requestId;
    }

    public function getData()
    {
        return $this->parsedData;
    }

    public function isOk()
    {
        return $this->isOk;
    }

}