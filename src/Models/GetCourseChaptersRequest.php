<?php
declare (strict_types=1);

namespace Yukebang\OpenApi\Models;

use Yukebang\OpenApi\Tea\Model;

/**
 * 获取课程章节列表
 */
class GetCourseChaptersRequest extends Model
{
    protected $version = 'v1';
    protected $path = '/course/{courseId}/chapters';
    protected $method = 'GET';

    public $courseId;

    public function validate()
    {
        Model::validateRequired('courseId', $this->courseId, true);
    }

    public function getPath(): string
    {
        return str_replace('{courseId}', $this->courseId, '/' . $this->version . $this->path);
    }

}