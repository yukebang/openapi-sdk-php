<?php
declare (strict_types=1);

namespace Yukebang\OpenApi\Models;

use Yukebang\OpenApi\Tea\Model;

/**
 * 获取课程播放地址
 */
class GetCoursePlaylistRequest extends Model
{
    protected $version = 'v1';
    protected $path = '/course/{courseId}/{chapterId}/playlist';
    protected $method = 'GET';

    public $courseId;

    public $chapterId;

    public function validate()
    {
        Model::validateRequired('courseId', $this->courseId, true);
        Model::validateRequired('chapterId', $this->chapterId, true);
    }

    public function getPath(): string
    {
        return str_replace([
            '{courseId}',
            '{chapterId}',
        ], [
            $this->courseId,
            $this->chapterId,
        ], '/' . $this->version . $this->path);
    }

}