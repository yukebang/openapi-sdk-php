<?php
declare (strict_types=1);

namespace Yukebang\OpenApi\Models;

use Yukebang\OpenApi\Tea\Model;

/**
 * 课程分类树形列表
 */
class GetCourseCategoryTreeRequest extends Model
{
    protected $version = 'v1';
    protected $path    = '/course/category-tree';
    protected $method  = 'GET';

}