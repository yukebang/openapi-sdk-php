<?php
declare (strict_types=1);

namespace Yukebang\OpenApi\Models;

use Yukebang\OpenApi\Tea\Model;

/**
 * 我采购的课程列表
 */
class ListCourseProcureRequest extends Model
{
    protected $version = 'v1';
    protected $path = '/course/procure-list';
    protected $method = 'GET';

    public $keywords;

    public $status = 1;

    public $page = 1;

    public $perPage = 15;

    protected $_name = [
        'keywords' => 'keywords',
        'status'   => 'status',
        'page'     => 'page',
        'perPage'  => 'per_page',
    ];
}