<?php
declare (strict_types=1);

namespace Yukebang\OpenApi\Models;

use Yukebang\OpenApi\Tea\Model;

/**
 * 我的课程列表
 */
class ListCourseRequest extends Model
{
    protected $version = 'v1';
    protected $path = '/course/list';
    protected $method = 'GET';

    public $keywords;

    public $status = 1;

    public $page = 1;

    public $perPage = 15;

    public $time;

    protected $_name = [
        'keywords' => 'keywords',
        'status'   => 'status',
        'time'     => 'time',
        'page'     => 'page',
        'perPage'  => 'per_page',
    ];
}