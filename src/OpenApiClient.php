<?php
declare (strict_types=1);

namespace Yukebang\OpenApi;

use GuzzleHttp\Client;
use Yukebang\OpenApi\Credentials\Credentials;
use Yukebang\OpenApi\Credentials\Signature;
use Yukebang\OpenApi\Exception\Exception;
use Yukebang\OpenApi\Exception\RequestException;
use Yukebang\OpenApi\Result\ResponseResult;
use Yukebang\OpenApi\Tea\Model;

class OpenApiClient
{
    // 默认版本
    const API_VERSION = '2024-01-01';

    // 默认接口地址
    const API_URI = 'openapi.yukebang.com';

    // 默认https协议
    const HTTPS_SCHEME = 'https';

    // 默认http协议
    const HTTP_SCHEME = 'http';

    // 默认超时时间
    protected $timeout = 10;

    // 是否使用https
    protected $useHttps = true;

    protected $customApiUri = null;

    private Credentials $credentials;

    public function __construct(Credentials $credentials)
    {
        $this->credentials = $credentials;
    }

    public function request(Model $model)
    {
        $model->validate();

        try {

            // 签名准备
            // 请求时间
            $ykbDate = date('Y-m-d\TH:i:s\Z');
            // 随机字符串
            $ykbNonce = bin2hex(random_bytes(16));
            // 请求头
            $headers = [
                'x-ykb-version'         => self::API_VERSION,
                'x-ykb-signature-nonce' => $ykbNonce,
                'x-ykb-date'            => $ykbDate,
                'content-type'          => 'application/json;charset=utf-8',
                'host'                  => $this->getApiUri(),
            ];

            // 签名
            $headers['authorization'] = Signature::getAuthorization($model, $this->credentials, $headers);

            $client = new Client([
                'base_uri' => $this->getRequestBaseUri(),
                'timeout'  => $this->timeout,
            ]);

            $response = $client->request($model->getMethod(), $model->getPath(), [
                'json'    => $model->toMap(),
                'headers' => $headers
            ]);

            return new ResponseResult($response);
        } catch (Exception $e) {
            throw new RequestException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getApiUri()
    {
        if ($this->getCustomApiUri()) {
            return $this->getCustomApiUri();
        }

        return self::API_URI;
    }

    public function getRequestBaseUri()
    {
        $uri = $this->getApiUri();
        if (stripos($uri, '://') !== false) {
            return $uri;
        }

        return sprintf('%s://%s', $this->useHttps ? self::HTTPS_SCHEME : self::HTTP_SCHEME, $this->getApiUri());
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function setTimeout(int $timeout): void
    {
        $this->timeout = $timeout;
    }

    public function isUseHttps(): bool
    {
        return $this->useHttps;
    }

    public function setUseHttps(bool $useHttps = true): void
    {
        $this->useHttps = $useHttps;
    }

    /**
     * @return null
     */
    public function getCustomApiUri()
    {
        return $this->customApiUri;
    }

    /**
     * @param null $customApiUri
     */
    public function setCustomApiUri($customApiUri): void
    {
        $this->customApiUri = $customApiUri;
    }

}