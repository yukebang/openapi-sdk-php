<?php
declare (strict_types=1);

namespace Yukebang\OpenApi\Credentials;

class Credentials
{
    private string $secretId;
    private string $secretKey;

    public function __construct(string $secretId, string $secretKey)
    {
        $this->secretId  = $secretId;
        $this->secretKey = $secretKey;
    }

    public function getSecretId(): string
    {
        return $this->secretId;
    }

    public function getSecretKey(): string
    {
        return $this->secretKey;
    }
}