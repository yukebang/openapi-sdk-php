<?php
declare (strict_types=1);

namespace Yukebang\OpenApi\Credentials;

use Yukebang\OpenApi\Tea\Model;

class Signature
{
    const SIGNATURE_ALGORITHM = 'YKB1-HMAC-SHA256';

    public static function getAuthorization(Model $model, Credentials $credentials, array $headers = []): string
    {
        $canonicalUri = $model->getPath();
        $canonicalUri = substr($canonicalUri, 0, 1) !== '/' ? '/' . $canonicalUri : $canonicalUri;

        $signHeaders      = self::getSignedHeaders($headers);
        $CanonicalRequest = strtoupper($model->getMethod()) . "\n" .
            $canonicalUri . "\n" .
            $model->getQuery() . "\n" .
            self::getCanonicalHeaders($headers) . "\n" .
            $signHeaders . "\n" .
            self::getHashedRequestPayload($model);

        $stringToSign = self::SIGNATURE_ALGORITHM . "\n" .
            self::hex($CanonicalRequest);

        $sign = bin2hex(hash_hmac('sha256', $stringToSign, $credentials->getSecretKey(), true));

        return sprintf(
            '%s Credential=%s,SignedHeaders=%s,Signature=%s',
            self::SIGNATURE_ALGORITHM,
            $credentials->getSecretId(),
            $signHeaders,
            $sign);

    }

    public static function getCanonicalHeaders(array $headers)
    {
        ksort($headers);

        $canonicalHeaders = [];
        foreach ($headers as $key => $value) {
            $canonicalHeaders[] = strtolower($key) . ':' . $value;
        }

        return implode("\n", $canonicalHeaders);
    }

    public static function getSignedHeaders(array $headers)
    {
        $signedHeaders = array_keys($headers);

        array_walk($signedHeaders, function (&$value) {
            $value = strtolower($value);
        });

        sort($signedHeaders);

        return implode(';', $signedHeaders);
    }

    public static function getHashedRequestPayload(Model $model)
    {
        $payload = $model->toMap();

        $payload = $payload ? self::ksortRecursive($payload) : [];

        $payloadStr = http_build_query($payload, '', '&', PHP_QUERY_RFC3986);

        return self::hex($payloadStr);

    }

    public static function ksortRecursive(array $array): array
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = self::ksortRecursive($value);
            }
        }

        ksort($array);

        return $array;
    }

    public static function hex($str)
    {
        return bin2hex(hash("SHA256", $str, true));
    }

}